export const regularExpressions = {
  userName: /^[ a-zA-Z0-9]*$/,
  email: /^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/,
  passWord: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
  confirmPassword: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
  firstName: /^[a-zA-Z]+$/,
  lastName: /^[a-zA-Z]+$/
};
