import React, { Component } from "react";
import "./App.scss";
import FormContainer from "../src/Containers/FormContainer/FormContainer";

class App extends Component {
  render() {
    return (
      <div className="wrapper">
        <FormContainer />
      </div>
    );
  }
}
export default App;
