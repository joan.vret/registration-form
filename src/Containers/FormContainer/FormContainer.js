import React, { Component } from "react";
import RegistrationDetails from "../../Components/RegistrationDetails/RegistrationDetails";
import PersonalDetails from "../../Components/PersonalDetails/PersonalDetails";
import Copyright from "../../Components/Copyright/Copyright";
import "./FormContainer.scss";
import {regularExpressions} from "../../helpers/regex";

const initialState = {
  user: {
    userName: "",
    email: "",
    passWord: "",
    confirmPassword: "",
    firstName: "",
    lastName: ""
  },

  inputErrors: {
    userName: " ",
    email: " ",
    passWord: " ",
    confirmPassword: " ",
    firstName: " ",
    lastName: " "
  },

  errorMessages: {
    userName: " Invalid username",
    email: " Invalid email ",
    passWord: "Invalid Psw ",
    confirmPassword: " Wrong Psw",
    firstName: " Invalid firstname",
    lastName: " Invalid lastname"
  },



  validFlags: {
    userName: false,
    email: false,
    passWord: false,
    confirmPassword: false,
    firstName: false,
    lastName: false,
    agreedValidTerms: false
  }
};

class FormContainer extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  checkValidation = (e, regEx) => {
    return regEx.test(e.target.value);
  };

  printError = (e, regEx, message) => {
    const inputErrors = this.state.inputErrors;
    const validFlags = this.state.validFlags;
    if (!this.checkValidation(e, regEx)) {
      inputErrors[e.target.name] = message;
      this.setState({ inputErrors });
    } else {
      inputErrors[e.target.name] = null;
      this.setState({ inputErrors });
      validFlags[e.target.name] = true;
      this.setState({ validFlags });
    }
  };

  handleInputChange = e => {
    e.preventDefault();
    const user = this.state.user;
    user[e.target.name] = e.target.value;
    this.setState({ user });
    this.printError(
      e,
      regularExpressions[e.target.name],
      this.state.errorMessages[e.target.name]
    );
  };

  handleCheckbox = e=> {
    this.setState({ checked: !this.state.checked });
  };

  handleSubmit = e => {
    e.preventDefault();
    if (this.state.validFlags) {
      alert("Congrats! You are signed up!");
      this.setState(initialState);
    }
  };

  render() {
    return (
      <div className="form-wrapper">
        <form onSubmit={this.handleSubmit}>
          <RegistrationDetails
            user={this.state.user}
            inputHandler={this.handleInputChange}
            inputErrors={this.state.inputErrors}
          />
          <PersonalDetails
            user={this.state.user}
            inputHandler={this.handleInputChange}
            inputErrors={this.state.inputErrors}
          />
          <Copyright validFlags={this.state.validFlags} />
        </form>
      </div>
    );
  }
}

export default FormContainer;
