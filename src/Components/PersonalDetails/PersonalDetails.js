import React, { Fragment as Aux } from "react";
import Title from "../Title/Title";
import InputField from "../InputField/InputField";
import Dropdown from "../Dropdown/Dropdown";
import "./PersonalDetails.scss";

const PersonalDetails = props => {
  return (
    <Aux>
      <Title h1_title="Personal Details" />
      <div className="name">
        <InputField
          divName="firstName"
          name="firstName"
          type="text"
          placeholder="Firstname"
          value={props.user.firstName}
          inputHandler={props.inputHandler}
          inputError={props.inputErrors.firstName}
        />
        <InputField
          divName="lastName"
          name="lastName"
          type="text"
          placeholder="Lastname"
          value={props.user.lastName}
          inputHandler={props.inputHandler}
          inputError={props.inputErrors.lastName}
        />
      </div>
      <div className="gender">
        <Dropdown />
      </div>
    </Aux>
  );
};

export default PersonalDetails;
