import React from "react";
import "./Checkbox.scss";

const Checkbox = ({ checked, change, label }) => {
  return (
    <div className="checkbox">
      <input type="checkbox" checked={checked} onChange={change} />
      <label className="checkbox--label" htmlFor="checkbox">
        {label}
      </label>
    </div>
  );
};

export default Checkbox;
