import React from "react";
import "./Dropdown.scss";

class Dropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: "Gender" };
  }

  change = e => {
    this.setState({ value: e.target.value });
  };

  render() {
    return (
      <select
        className="gender"
        value={this.state.value}
        onChange={e => this.change(e)}
      >
        <option value="Gender">Gender</option>
        <option className="option" value="male">
          Male
        </option>
        <option className="option" value="female">
          Female
        </option>
        <option className="option" value="other">
          Other
        </option>
      </select>
    );
  }
}

export default Dropdown;
