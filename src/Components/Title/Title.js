import React from "react";
import "./Title.scss";

const Title = ({ h1_title }) => {
  return <h1>{h1_title}</h1>;
};

export default Title;
