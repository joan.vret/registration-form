import React, { Fragment as Aux } from "react";
import Checkbox from "../Checkbox/Checkbox";
import "./Copyright.scss";

const Copyright = props => {
  return (
    <Aux>
      <div className="checkboxContainer">
        <Checkbox label="I want to receive news and special offers" />
        <Checkbox
          label="I agree with the Terms and Conditions"
          checked={props.validFlags.checked}
          change={props.handleCheckbox}
        />
      </div>
      <div className="submit">
        <button type="submit">
          Submit
        </button>
      </div>
    </Aux>
  );
};
export default Copyright;
