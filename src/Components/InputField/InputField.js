import React from "react";
import "./InputField.scss";

const InputField = ({
  divName,
  name,
  materialName,
  type,
  placeholder,
  value,
  inputHandler,
  inputError
}) => {
  return (
    <div className={divName}>
      {renderIcon(materialName)}
      <input
        name={name}
        type={type}
        placeholder={placeholder}
        defaultValue={value}
        onChange={e => inputHandler(e)}
        required
      />
      {renderError(inputError)}
    </div>
  );
};

const renderIcon = materialName =>
  materialName ? <i className="material-icons">{materialName}</i> : null;

const renderError = inputError =>
  inputError ? <div className="error">{inputError}</div> : null;

export default InputField;
