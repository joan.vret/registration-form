import React, { Fragment as Aux } from "react";
import Title from "../Title/Title";
import InputField from "../InputField/InputField";
import "./RegistrationDetails.scss";

// const usernameRegex = /^[ a-zA-Z0-9]*$/;
// const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
// const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const RegistrationDetails = props => {
  return (
    <Aux>
      <Title h1_title="Registration Form" />
      <InputField
        divName="userName"
        materialName="account_box"
        name="userName"
        type="text"
        placeholder="Username"
        value={props.user.userName}
        inputHandler={props.inputHandler}
        inputError={props.inputErrors.userName}
      />
      <InputField
        divName="emailAddress"
        materialName="email"
        name="email"
        type="email"
        placeholder="Email Address"
        required
        className="form-control"
        value={props.user.email}
        inputHandler={props.inputHandler}
        inputError={props.inputErrors.email}
      />
      <div className="PasswordContainer">
        <InputField
          divName="passWord"
          materialName="lock_open"
          name="passWord"
          type="password"
          placeholder="Password"
          value={props.user.passWord}
          inputHandler={props.inputHandler}
          inputError={props.inputErrors.password}
        />
        <InputField
          name="confirmPassword"
          type="password"
          placeholder="Confirm Password"
          value={props.user.confirmPassword}
          inputHandler={props.inputHandler}
          inputError={props.inputErrors.confirmPassword}
        />
      </div>
    </Aux>
  );
};
export default RegistrationDetails;
